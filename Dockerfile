FROM python:3.9-slim-buster
WORKDIR /build
RUN apt-get update && \
    apt-get -y install git && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge -y --auto-remove && \
    apt-get clean && \
    rm -rf /var/cache/apt/archives
