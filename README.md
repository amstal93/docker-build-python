# Docker build image: Python

Docker image with Python / Pip geared towards builds.

## Quickref

```sh
docker run --rm -v $(pwd):/build \
  registry.gitlab.com/sjugge/docker/build-images/docker-build-python \
  /bin/bash -c "python -m venv .venv-build && source .venv-build/bin/activate && pip install -r requirements.txt && make build && rm -rf .venv-build"
```
